import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./css/App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import { EventScreen } from "./EventScreen.tsx";

import EventCards from "./menuComponents/Cards.tsx";
import Layout from "./menuComponents/LayoutNavBar.tsx";
import CreateEvent from "./createFromComponents/CreateEvent.tsx";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  const [count, setCount] = useState(0);

  return (
    <BrowserRouter>
      <Layout>
        <Routes>
          <Route index element={<EventCards />} />
          <Route path="/blank-page" element={<EventScreen />} />
          <Route path="/createEvent" element={<CreateEvent />} />
        </Routes>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
