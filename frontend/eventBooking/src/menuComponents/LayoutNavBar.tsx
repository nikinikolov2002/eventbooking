import React from "react";
import NavBar from "./NavBar";
import LayoutProps from "../interfaces/Layout";

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <>
      <NavBar />
      {children}
    </>
  );
};

export default Layout;
