import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import photo from "../assets/photo1.jpg";
import "../css/components.css";
import { Fragment, useEffect, useState } from "react";
import Event from "../interfaces/Event";

import CardGroup from "react-bootstrap/CardGroup";
import { fetchEvents } from "../fetch/FetchEventsFromApi";
import { Link } from "react-router-dom";

function EventCards() {
  const [allEvents, setEvents] = useState<Event[]>([]);

  const getEvents = async () => {
    try {
      const data = await fetchEvents();
      setEvents(data);
    } catch (err) {}
  };
  useEffect(() => {
    getEvents();
  }, []);

  return (
    <div className="media">
      {allEvents.map((event, rowIndex) => (
        <CardGroup key={rowIndex}>
          <Card>
            <Card.Img variant="top" src={photo} />
            <Card.Body>
              <Card.Title>{event.title}</Card.Title>
              <Card.Text className="text">{event.description}</Card.Text>
              <Link to="/blank-page">
                <Button variant="outline-dark">Go to event</Button>
              </Link>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">Last updated {}</small>
            </Card.Footer>
          </Card>
        </CardGroup>
      ))}
    </div>
  );
}

export default EventCards;
