import Event from "../interfaces/Event";

export async function fetchEvents(): Promise<Event[]> {
  const response = await fetch("http://localhost:8084/evently/event/all");

  if (!response.ok) {
    throw new Error(`Failed to fetch events: ${response.statusText}`);
  }

  const data: Event[] = await response.json();
  return data;
}
