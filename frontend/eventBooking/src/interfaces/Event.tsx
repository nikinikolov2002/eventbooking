export default interface Event {
  title: string;
  description: string;
  startDate: Date;
  endDate: Date;
  status: string | undefined;
  notification: boolean;
}
