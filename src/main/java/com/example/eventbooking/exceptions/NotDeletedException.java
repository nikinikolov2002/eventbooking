package com.example.eventbooking.exceptions;

public class NotDeletedException extends RuntimeException{

    public NotDeletedException(String message){
        super(message);
    }
}
