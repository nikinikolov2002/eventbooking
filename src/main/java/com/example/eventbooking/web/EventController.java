package com.example.eventbooking.web;

import com.example.eventbooking.dto.event.EventCreateDto;
import com.example.eventbooking.dto.event.EventDto;
import com.example.eventbooking.exceptions.NotDeletedException;
import com.example.eventbooking.model.Event;
import com.example.eventbooking.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/evently/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @GetMapping(value = "all")
    public ResponseEntity<List<EventDto>> seeEvent(){
        return ResponseEntity.ok().body(eventService.getAllEvents());
    }
    @PostMapping(value = "create")
    public ResponseEntity<EventDto> createEvent(@RequestBody EventCreateDto eventDto){
        return ResponseEntity.ok(eventService.createEvent(eventDto));
    }
    @DeleteMapping(value = "/delete/{eventId}")
    public ResponseEntity<String> deleteEvent(@PathVariable Long eventId) {
        try {
            eventService.deleteEventById(eventId);
            return ResponseEntity.ok("Event deleted successfully");
        } catch (NotDeletedException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
