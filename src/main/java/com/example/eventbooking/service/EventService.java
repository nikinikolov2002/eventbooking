package com.example.eventbooking.service;

import com.example.eventbooking.dto.event.EventCreateDto;
import com.example.eventbooking.dto.event.EventDto;
import com.example.eventbooking.exceptions.InvalidObjectException;
import com.example.eventbooking.exceptions.NotDeletedException;
import com.example.eventbooking.mapping.EventMapper;
import com.example.eventbooking.model.Event;
import com.example.eventbooking.repository.EventRepository;
import com.example.eventbooking.validation.ObjectValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Component
@Service
public class EventService {
    @Autowired
    private EventRepository repo;
    @Autowired
    private EventMapper mapper;
    @Autowired
    private ObjectValidator validator;

    public List<EventDto>getAllEvents(){
        List<Event>events = (List<Event>) repo.findAll();
        return mapper.responseFromModelDtoList(events);
    }
    public EventDto createEvent(EventCreateDto createDto){
        Map<String, String> validationErrors = validator.validate(createDto);
        if (validationErrors.size() != 0) {
            throw new InvalidObjectException("Invalid Customer Create", validationErrors);
        }
        Event event = mapper.modelFromCreateRequest(createDto);
        return mapper.responseFromModelDto(repo.save(event));
    }
    public void deleteEventById(Long id) {
        Boolean isDeleted = repo.deleteEventById(id);
        if(!isDeleted){
            throw new NotDeletedException("Cannot be deleted!");
        }
    }



}
