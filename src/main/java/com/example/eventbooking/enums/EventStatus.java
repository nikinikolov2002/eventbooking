package com.example.eventbooking.enums;

public enum EventStatus {
    DRAFT,
    PUBLISHED,
    CANCELLED
}