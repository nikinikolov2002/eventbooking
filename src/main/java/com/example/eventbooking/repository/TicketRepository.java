package com.example.eventbooking.repository;

import com.example.eventbooking.model.Event;
import com.example.eventbooking.model.Ticket;
import org.springframework.data.repository.CrudRepository;

public interface TicketRepository extends CrudRepository<Ticket, Long> {
    void deleteTicketById(Long id);
}
