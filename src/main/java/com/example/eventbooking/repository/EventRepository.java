package com.example.eventbooking.repository;

import com.example.eventbooking.model.Event;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface EventRepository  extends CrudRepository<Event, Long> {

    Boolean deleteEventById(Long id);

}
