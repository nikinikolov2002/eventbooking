package com.example.eventbooking.dto.event;

import com.example.eventbooking.enums.EventStatus;
import com.example.eventbooking.model.Category;
import com.example.eventbooking.model.Ticket;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
@Builder
public class EventDto {

    private String title;
    private String description;

    private LocalDate date;
    private LocalTime time;

    private String location;
    private Integer ticketPrice;

    private String categoryType;
    private Integer ticketsCount;
    @Enumerated(EnumType.STRING)
    private EventStatus status;
}
