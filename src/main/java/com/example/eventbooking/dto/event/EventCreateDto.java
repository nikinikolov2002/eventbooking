package com.example.eventbooking.dto.event;


import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
public class EventCreateDto {

    private String title;
    private String description;

    private LocalDate date;
    private LocalTime time;

    private String location;
}
