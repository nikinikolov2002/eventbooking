package com.example.eventbooking.dto.ticket;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TicketCreateDto {

    private String ticketType;

    private Double price;

}
