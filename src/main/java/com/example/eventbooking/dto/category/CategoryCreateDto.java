package com.example.eventbooking.dto.category;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CategoryCreateDto {

    private String categoryName;
}
