package com.example.eventbooking.mapping;


import com.example.eventbooking.dto.event.EventCreateDto;
import com.example.eventbooking.dto.event.EventDto;
import com.example.eventbooking.dto.ticket.TicketCreateDto;
import com.example.eventbooking.dto.ticket.TicketDto;
import com.example.eventbooking.model.Event;
import com.example.eventbooking.model.Ticket;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface TicketMapper {

    TicketDto responseFromModelDto(Ticket ticket);

    Ticket modelFromCreateRequest(TicketCreateDto ticketCreateDto);
}
