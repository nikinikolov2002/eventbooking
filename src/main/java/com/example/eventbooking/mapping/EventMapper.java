package com.example.eventbooking.mapping;

import com.example.eventbooking.dto.event.EventCreateDto;
import com.example.eventbooking.dto.event.EventDto;
import com.example.eventbooking.model.Event;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface EventMapper {

     EventDto responseFromModelDto(Event event);
     List<EventDto>responseFromModelDtoList(List<Event> events);

     Event modelFromCreateRequest(EventCreateDto eventDto);


}
