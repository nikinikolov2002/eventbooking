package com.example.eventbooking.mapping;


import com.example.eventbooking.dto.category.CategoryCreateDto;
import com.example.eventbooking.dto.category.CategoryDto;
import com.example.eventbooking.dto.event.EventCreateDto;
import com.example.eventbooking.model.Category;
import com.example.eventbooking.model.Event;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CategoryMapper {

    CategoryDto responseFromModelDto(Category category);

    Category modelFromCreateRequest(CategoryCreateDto categoryDto);
}
